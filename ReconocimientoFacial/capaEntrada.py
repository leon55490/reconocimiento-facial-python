import cv2
# ? Crear carpetas. archivos a traves de codigo python
import os
# ? bajar la calidad para que no ocupe mucha memoria
import imutils

# ? Crear la carpeta
modelo = 'Auron'
ruta1 = "C:/Users/DAVID/Downloads/reconocimiento-facial-python-main/ReconocimientoFacial 1"
rutacompleta = ruta1 + '/' + modelo

# ? Mirar si la ruta existe, y si no existe, crear la carpeta
if not os.path.exists(rutacompleta):
    os.makedirs(rutacompleta)

camara = cv2.VideoCapture(
    'C:/Users/DAVID/Downloads/reconocimiento-facial-python-main/ReconocimientoFacial 1/videoauron.mp4')
# ? Cargar los ruidos
ruidos = cv2.CascadeClassifier(
    cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
# ? Identificador de cada imagen, se va a ir incrementando
id = 0


while True:
    respuesta, captura = camara.read()
    # ? Si no encuentra la variable respuesta, terminar todo el proceso
    if respuesta == False:
        break
    # ? Cambiar la resolucion de la camara
    captura = imutils.resize(captura, width=640)
    grises = cv2.cvtColor(captura, cv2.COLOR_BGR2GRAY)
    # ? Comparar ruidos con la imagen que tendra el rostro (los porcentajes se representan con un 1, por ejemplo para representar el 50% seria 1.5)
    # ? Influye el ambiente, luces, ruido, sombras, muy importante al momento de reconocer un rostro

    cara = ruidos.detectMultiScale(grises, 1.3, 5)

    # ? capturar cada punto de la imagenes, optimiza procesos, porque utiliza informacion ya procesada
    idcaptura = captura.copy()

    # ? ciclo para que analice toda la cara, x, y, las esquinas
    for(x, y, e1, e2) in cara:
        # ? Hacer un rectangulo
        cv2.rectangle(captura, (x, y), (x + e1, y + e2), (255, 0, 0), 2)
        # ? Saca fragmentos de los rostros y los almacena en la carpeta
        rostrocapturado = idcaptura[y:y+e2, x:x+e1]
        # ? imagen a escalar, la imagen se vea lo mejor posible, sin ser demasiado pesado
        rostrocapturado = cv2.resize(
            rostrocapturado, (160, 160), interpolation=cv2.INTER_CUBIC)
        # ? Modificar cada nombre de las imagenes que se van agregando, para poder diferenciar las imagenes
        cv2.imwrite(rutacompleta + '/imagen_{}.jpg'.format(id),
                    rostrocapturado)
        id = id + 1

    cv2.imshow("resltado rostro", captura)

    if id == 501:
        break
camara.release()
cv2.destroyAllWindows()
