import cv2
import os
import imutils

dataRuta = 'C:/Users/DAVID/Downloads/reconocimiento-facial-python-main/ReconocimientoFacial 1/Data'
listaData = os.listdir(dataRuta)

entrenamientoEigenFaceRecognizer = cv2.face.LBPHFaceRecognizer_create()
entrenamientoEigenFaceRecognizer.read(
    'C:/Users/DAVID/Downloads/reconocimiento-facial-python-main/ReconocimientoFacial 1/EntrenamientoEigenFaceRecognizer.xml')
ruidos = cv2.CascadeClassifier(
    cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
camara = cv2.VideoCapture(
    'C:/Users/DAVID/Downloads/reconocimiento-facial-python-main/ReconocimientoFacial 1/ElonMusk.mp4')

while True:
    respuesta, captura = camara.read()
    if respuesta == False:
        break
    # ? cambiar la ventana del video, el tamaño
    captura = imutils.resize(captura, width=680)
    grises = cv2.cvtColor(captura, cv2.COLOR_BGR2GRAY)
    idcaptura = grises.copy()
    cara = ruidos.detectMultiScale(grises, 1.3, 5)
    for(x, y, e1, e2) in cara:
        rostrocapturado = idcaptura[y:y+e2, x:x+e1]
        rostrocapturado = cv2.resize(
            rostrocapturado, (160, 160), interpolation=cv2.INTER_CUBIC)
        # ? comparar el modelo aprendido, con lo capturado, cada punto se compara, para obtener la red neuronal, saber si se parece un rostro o no
        resultado = entrenamientoEigenFaceRecognizer.predict(rostrocapturado)
        # ? poner el texto
        cv2.putText(captura, '{}'.format(resultado), (x, y-5),
                    1, 1.3, (0, 255, 0), 1, cv2.LINE_AA)
        # ? Ponerle nombre a la persona identificada
        if resultado[1] < 1:
            cv2.putText(captura, "No encontrado", (x, y-20),
                        2, 1.1, (0, 255, 0), 1, cv2.LINE_AA)
            # ? Cerrar el rectangulo
            cv2.rectangle(captura, (x, y), (x+e1, y+e2), (0, 0, 255), 2)
        else:
            cv2.putText(captura, '{}'.format(
                listaData[resultado[0]]), (x, y-20), 2, 0.7, (0, 255, 0), 1, cv2.LINE_AA)
            # ? Cerrar el rectangulo
            cv2.rectangle(captura, (x, y), (x+e1, y+e2), (0, 0, 255), 2)

    cv2.imshow("Resultados", captura)
    if cv2.waitKey(1) == ord('q'):
        break
camara.release()
cv2.destroyAllWindows()
