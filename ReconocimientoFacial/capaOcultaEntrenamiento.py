import cv2
import os
import numpy as np
# ? Ver el tiempo que tardan los procesos
from time import time

dataRuta = 'C:/Users/DAVID/Downloads/reconocimiento-facial-python-main/ReconocimientoFacial 1/Data'

# ? crear una lista con la informacion
listaData = os.listdir(dataRuta)
# ? print('data', listaData) (para saber si reconocio la informacion, que si se hizo la lista)

# ? identificar cada imagen, ponerle una etiqueta, un id
ids = []
rostroData = []
id = 0

# ? Ver el tiempo que se va a tardar el proceso, en segundos
tiempoInicial = time()

# ? Recorrer toda la informacion como lista
for fila in listaData:
    rutacompleta = dataRuta + '/' + fila
    print('Iniciando lectura...')
    for archivo in os.listdir(rutacompleta):
        # ? para saber en que proceso me encuentro
        print('imagenes: ', fila + '/' + archivo)
        ids.append(id)
        # ? ubicar la ruta de la imagen y pasarlo a una escala de gris
        rostroData.append(cv2.imread(rutacompleta + '/' + archivo, 0))

    # ? ir clasificando cada imagen
    id = id + 1
    # ? Ver el tiempo de lectura del procesos
    tiempofinalLectura = time()
    tiempoTotalLectura = tiempofinalLectura - tiempoInicial
    print('Tiempo total de lectura: ', tiempoTotalLectura)

# ? Pack de modelado de imagenes (entrenamientos)
entrenamientoEigenFaceRecognizer = cv2.face.LBPHFaceRecognizer_create()
print('iniciando el entrenamiento.....espere')
# ? asignarle datos, agregar la informacion que se va a recorrer
entrenamientoEigenFaceRecognizer.train(rostroData, np.array(ids))
# ? Ver el tiempo que se ha tardado desde que se inicio el entrenamiento
tiempofinalEntrenamiento = time()
tiempoTotalEntrenamiento = tiempofinalEntrenamiento - tiempoTotalLectura
print('Tiempo entrenamiento total ', tiempoTotalEntrenamiento)
# ? Guardar la informacion, crear la primera red, neuronal, crear el archivo
entrenamientoEigenFaceRecognizer.write('EntrenamientoEigenFaceRecognizer.xml')
print('Entrenamiento concluido')
