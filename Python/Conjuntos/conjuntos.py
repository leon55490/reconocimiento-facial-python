# conjuntos

A = {1, 2, 3, 4}
B = {2, 3, 5, 6}
C = {3, 4, 6, 7}

# Union de conjuntos
print(A | B)

# Interseccion de conjuntos
print(A & B)

# Diferencia de conjuntos
print(A - B)

# Diferencia simetrica
print(A ^ B)
