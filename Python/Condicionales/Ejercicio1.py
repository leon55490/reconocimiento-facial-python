# Primer ejercicio

primero = int(input("Ingrese el primer numero: "))
segundo = int(input("Ingrese el segundo numero: "))

if primero % 2 == 0 and segundo % 2 == 0:
    print("Ambos son pares")
elif primero % 2 == 0 and segundo % 2 != 0:
    print(f"{primero} es un numero par")
elif primero % 2 != 0 and segundo % 2 == 0:
    print(f"{segundo} es un numero par")
else:
    print("los dos numeros son impares")
