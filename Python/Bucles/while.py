# Ciclo While
# El hogar de miss peregrine para niños peculiares

# numero = 0
# while numero < 20:
#     print(numero)
#     numero += 1

import math

numero = int(input("Escriba un numero: "))

while numero < 0:
    print("Ingrese un numero positivo")
    numero = int(input("Vuelva a ingresar un numero positivo: "))

print(f"El resultado de la raiz cuadrada es:{math.sqrt(numero):.2f}")
