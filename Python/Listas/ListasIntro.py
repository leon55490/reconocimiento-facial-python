# Introduccion a listas

# array = ["futbol", "pc", 18.6, 18, [6, 7, 10, 4], True, False]
# print(array[:3])
# print(len(array))
# array.append(66)
# array.append(True)
# array.insert(1, 88)
# array.extend([1.88, True, 100])

# array1 = ["futbol", "pc", 18.6, 18, [6, 7, 10, 4], True, False]
# array2 = [100, 250, "hola"]
# array3 = array1 + array2
# print(array3)

# array = ["futbol", "pc", 18.6, 18, [6, 7, 10, 4], True, False]
# print("pc" in array)

#array = ["futbol", "pc", 18.6, 18, [6, 7, 10, 4], True, False, "pc"]
# print(array.index("pc"))
# print(array.count("pc"))
# array.remove("pc")
# array.clear()

array = [1, 2, 5, -11.8]
# array.sort()
array.sort(reverse=True)
print(array)
